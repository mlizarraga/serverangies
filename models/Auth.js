const MYSQL= require('../bd/connection');
module.exports=class Auth{
    table
    constructor(table){
        this.table=table;
    }
    getUser(conditions,success){
        MYSQL.query(`select id,idPerfil,idSucursal,usuario from ${this.table} where usuario=? and password=?`,conditions,(err,rows,fields)=>{
            if(err) throw err;
            success(rows);
        })
    }
    
    logOut(data,success){
        MYSQL.query(`update ${this.table} set logIn=0 where id=?`,[data],(err,result)=>{
            if(err) throw err;
            success(result.affectedRows);
        });
    }
    saveLogIn(data){
        MYSQL.query(`insert into logueos set ?`,data,(err,rows,fields)=>{
            MYSQL.query(`update ${this.table} set logIn=1 where id=?`,[data.idUsuario],(err,result)=>{});
        });
    }
    getModules(id,success){
        let query=`SELECT secciones.descripcion,secciones.icono AS seccionicon,modulos.descripcion,modulos.id,modulos.icono AS iconomodulo FROM permisos
        INNER JOIN seccionesmodulos ON seccionesmodulos.idModulo=permisos.idModulo
        INNER JOIN modulos ON modulos.id=seccionesmodulos.idModulo
        INNER JOIN secciones ON secciones.id=seccionesmodulos.idSeccion
        WHERE idPerfil=?
        ORDER BY secciones.descripcion`;
        MYSQL.query(query,id,(err,rows,fields)=>{
            if(err) throw err;
            success(rows);
        });
    }
}
