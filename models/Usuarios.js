const MYSQL= require('../bd/connection');
module.exports=class Usuarios{
    table;
    constructor(){
        this.table="Usuarios";
    }
    getModules(where,success){
        let query=`SELECT secciones.id as idSeccion,secciones.descripcion AS seccion,modulos.id as idModulo,modulos.descripcion AS modulo,modulos.url,modulos.icon FROM permisos
        INNER JOIN modulos ON modulos.id=permisos.idModulo
        INNER JOIN seccionesmodulos ON seccionesmodulos.idModulo=permisos.idModulo
        INNER JOIN secciones ON secciones.id=seccionesmodulos.idSeccion
        WHERE idPerfil=?`
        MYSQL.query(query,where,(err,rows)=>{
            if(err) throw err;
            success(rows);
        });
    }
}