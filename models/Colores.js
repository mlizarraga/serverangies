const MYSQL= require('../bd/connection');
module.exports=class Colores{
    table
    constructor(){
        this.table="colores";
    }
    where(search){
        let conditional="where  status=1 ";
        if(Object.keys(search).length>0){
            conditional+=' and '
            Object.keys(search).map((row)=>{ 
                conditional+=` ${row} like "%${search[row]}%" AND `;
            });
            return conditional.substr(0,conditional.length-4);
        } else
            return " where status=1 ";
    }
    getAll(data,success){
        let search=JSON.parse(data.search);
        MYSQL.query(`select id,descripcion from ${this.table} ${this.where(search)}  limit ${data.initial},${data.offset}` ,search,(err,rows,fields)=>{
            if(err)
                throw err;
            this.getRows(rows,data,success)
            //return success(rows);    
        })
    }
    save(data,success){
        data.data['creadoPor']=data.id;
        MYSQL.query(`insert into ${this.table} set ?`,data.data,(err,result)=>{
            if(err) throw err;
            data.data.id=result.insertId;
            return success(data.data);
        })
    }
    update(id,data,success){
        data.data['modificadoPor']=data.id;
        data.data['fechaModificacion']=new Date();
        MYSQL.query(`update ${this.table} set ? where id=?`,[data.data,id],(err,result)=>{
            if(err) throw err;
            return success(data.data)
        })
    }
    delete(ids,data,success){
        ids=JSON.parse(ids);
        MYSQL.query(`update ${this.table} set status=0,modificadoPor=${data.id}, fechaModificacion=curdate() where id in(?)`,[ids],(err,result)=>{
            if(err) throw err;
            return success(data.data)
        })
    }
    getRows(rows,data,success){
        let search=JSON.parse(data.search);
        MYSQL.query(`select id,descripcion from ${this.table} ${this.where(search)}`,search,(err,numRows,fields)=>{
            if(err)
                throw err;
            return success({data:rows,rows:numRows.length});    
        })
    }
}