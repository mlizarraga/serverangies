const PORT=9000;
const MSQL=require('./bd/connection');
let EXPRESS=require('express');
const LOGIN= require('./routes/login');
const USUARIOS=require('./routes/usuarios');
const COLORES=require('./routes/colores');
const CORS= require('cors');/**avoid problems with Access-Controll-Allow-Origin */
let APP=EXPRESS();
APP.use(CORS());
APP.use(EXPRESS.json());
APP.use(EXPRESS.urlencoded({extended:true}));
APP.use('/api',LOGIN);
APP.use('/api',USUARIOS);
APP.use('/api',COLORES);

APP.get('/',(req,res)=>{
    res.json({title:'Hola mundo'})
});
APP.listen(PORT,()=>{
    console.log(`Initializing server at port ${PORT}`);
}); 