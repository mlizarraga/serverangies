const EXPRESS= require('express');
const USUARIOMODEL= require('../models/Usuarios');

const userm= new USUARIOMODEL();
const USUARIO=EXPRESS.Router();
const CHECKTOKENMIDDELWARE=require('../middlewares/checkToken');
const middleware=(req,res,next)=>{
    if(Object.keys(req.body).length !== 0 )
        next();
    else
        res.status(403).send({errorMessage:'You need a payload'});
}
USUARIO.route('/usuario')
.post([middleware,CHECKTOKENMIDDELWARE],(req,res)=>{
    let {id} = req.user;
    userm.getModules([id],(rows)=>{
        res.status(200).send(rows);
    })
});
module.exports=USUARIO;