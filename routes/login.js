const EXPRESS=require('express');
const IP=require('ip');
const JWT= require('jsonwebtoken');
const CHECKTOKENMIDDELWARE=require('../middlewares/checkToken');
const SECRET= require('../security/secret');
const  md5 = require('md5');
const AUTH=require('../models/Auth');
const LOGIN=EXPRESS.Router();

let _auth= new AUTH('usuarios');
const middleware=(req,res,next)=>{
    if(Object.keys(req.body).length !== 0 )
        next();
    else
        res.status(403).send({errorMessage:'You need a payload'});
}
LOGIN.route('/login')
.post(middleware,(req,res)=>{
    let {usuario,password} = req.body;
    let token="";
    _auth.getUser([usuario,md5(password)],(rows)=>{
        if(rows.length>0){
            token=JWT.sign({id:rows[0].id,usuario:rows[0].usuario,idPerfil:rows[0].idPerfil,idSucursal:rows[0].idSucursal},SECRET);
            _auth.saveLogIn({ip:IP.address(),idUsuario:rows[0].id,usuario:rows[0].usuario});
            _auth.getModules(rows[0].id,(result)=>{
                res.status(200).send({logIn:true,token:token,modules:result});
            });
           
        }else{
            _auth.logOut(req.body.id);
            res.status(200).send({logIn:false,token:""});
        }
            
    });
});
LOGIN.route('/logout')  
.get(CHECKTOKENMIDDELWARE,(req,res)=>{
    _auth.logOut(req.user.id,(response)=>{
        if(response>0)
            res.status(200).send({logIn:false,message:'****'});
        else
            res.status(200).send({logIn:true,message:'Please try again'});
    })
})
module.exports=LOGIN;