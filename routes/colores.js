const EXPRESS= require('express');
const _COLORES= require('../models/Colores');
const mColores=new _COLORES();
const CHECKTOKENMIDDELWARE=require('../middlewares/checkToken');
const COLORES=EXPRESS.Router();
const middleware=(req,res,next)=>{
    if(Object.keys(req.body).length !== 0 )
        next();
    else
        res.status(403).send({errorMessage:'You need a payload'});
}
COLORES.route('/colores')
.get(CHECKTOKENMIDDELWARE,async (req,res)=>{
    let {id,idPerfil}=req.user;
    let {initial,offset,search}=req.query;
    mColores.getAll({id,idPerfil,initial,offset,search},(rows)=>{
        res.json({data:rows.data,rows:rows.rows});
    })
})
.post([middleware,CHECKTOKENMIDDELWARE],(req,res)=>{
    let {id,idPerfil}=req.user;
    let data=req.body;
    mColores.save({id,idPerfil,data},(result)=>{
        res.json(result);
    });
})
;

COLORES.route('/colores/:id')
.patch([middleware,CHECKTOKENMIDDELWARE],(req,res)=>{
    let {id,idPerfil}=req.user;
    let data=req.body;
    mColores.update(req.params.id,{id,idPerfil,data},(result)=>{
        res.json(result); 
    });
})
.delete([CHECKTOKENMIDDELWARE],(req,res)=>{
    let {id,idPerfil}=req.user;
    let ids=req.params.id;
    mColores.delete(ids,{id,idPerfil},(result)=>{
        res.json(result);
    });
});

module.exports=COLORES;