const JWT=require('jsonwebtoken');
const SECRET= require('../security/secret');
module.exports=(req,res,next)=>{
    let authorization=req.headers.authorization;
    if(authorization){
        const token=authorization.split(' ')[1];
        JWT.verify(token,SECRET,(err,user)=>{
            if(err)
                return res.sendStatus(401);
            else{
                req.user=user;
                next();
            }
        })
    }else
        res.sendStatus(401);
}